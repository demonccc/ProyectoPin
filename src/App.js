import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import BarMenu from './Components/BarMenu';

function App() {
  return (
   <div>
    <BarMenu></BarMenu>
   </div>
  );
}

export default App;
